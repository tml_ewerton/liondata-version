<?php

namespace Lion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerContact extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'customer_id', 'type', 'name', 'value', 'note'
    ];

    /**
     * Relationship with Customer.
     * CustomerContact belongs to Customer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo('Lion\Customer');
    }

}
