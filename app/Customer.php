<?php

namespace Lion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'name', 'company_name', 'document', 'picture', 'about', 'slug',
        'website'
    ];

    /**
     * Relationship with CustomerAddress.
     * Customer has many CustomerAddress.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function customerAddress()
    {
        return $this->hasMany('Lion\CustomerAddress');
    }

    /**
     * Relationship with CustomerContact.
     * Customer has many CustomerContact.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function customerContact()
    {
        return $this->hasMany('Lion\CustomerContact');
    }

}
