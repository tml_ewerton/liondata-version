<?php

namespace Lion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerAddress extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'customer_id', 'type', 'address', 'number', 'complement', 'neighborhood', 'city',
        'state', 'zip_code', 'code'
    ];

    /**
     * Relationship with Customer.
     * CustomerAddress belongs to Customer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo('Lion\Customer');
    }
}
