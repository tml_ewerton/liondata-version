<?php

namespace Lion\Http\Controllers;

use Illuminate\Http\Request;
use Lion\CustomerAddress;
use Lion\Services\DataLayer;

class CustomerAddressController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = CustomerAddress::create($request->except('_token'));
        return $customer ? redirect()->to(
            '/dashboard/customers/' . $customer->id
        ) : back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Lion\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerAddress $customer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Lion\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomerAddress $customer)
    {
        //
    }

    public function data(Request $request)
    {
        $dataLayer = new DataLayer(new CustomerAddress());
        return response()->json($dataLayer->execute($request));
    }

}
