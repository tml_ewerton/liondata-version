<?php

namespace Lion\Http\Controllers;

use Illuminate\Http\Request;

class WebhookController extends Controller
{

    public function facebook(Request $request)
    {
        if ($request->hub_verify_token === 'ldo') {
            return $request->hub_challenge;
        }
    }

}
