<?php

namespace Lion\Http\Controllers;

use Illuminate\Http\Request;
use Lion\CustomerContact;
use Lion\Services\DataLayer;

class CustomerContactController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = CustomerContact::create($request->except('_token'));
        return $customer ? redirect()->to(
            '/dashboard/customers/' . $customer->id
        ) : back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Lion\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerContact $customer)
    {
        $response = CustomerContact::findOrFail($customer->id)->update(
            $request->except('_token')
        );

        return response()->json([$response, $request->except('_token')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Lion\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomerContact $customer)
    {
        $response = CustomerContact::delete($customer->id);
        return response()->json($response);
    }

    public function data(Request $request)
    {
        $dataLayer = new DataLayer(new CustomerContact());
        return response()->json($dataLayer->execute($request));
    }
}
