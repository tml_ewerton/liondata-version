<?php

namespace Lion\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Lion\Customer;
use Illuminate\Http\Request;
use Lion\Services\DataLayer;

class CustomerController extends Controller
{

    private $relationship = [
        'customerAddress', 'customerContact'
    ];

    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.customers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('picture')) {
            $file = $request->file('picture');
            unset($request->picture);
            $filename = 'file-'. md5(time() . uniqid()) . '.' .
                        $file->getClientOriginalExtension();
            $path = $file->storeAs('/customers/avatars', $filename);
        }
        $data = $request->except('_token', 'picture');
        $data['picture'] = '/' . $path;

        $customer = Customer::create($data);
        return $customer ? redirect()->to(
            '/dashboard/customers/' . $customer->id
        ) : back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \Lion\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        return view('dashboard.customers.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Lion\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        return view('dashboard.customers.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Lion\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $response = Customer::findOrFail($customer->id)->update(
            $request->except('_token')
        );

        return response()->json([$response, $request->except('_token')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Lion\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $response = Customer::find($customer->id)->delete();
        return response()->json($response);
    }

    public function data(Request $request)
    {
        $dataLayer = new DataLayer(new Customer(), $this->relationship);
        return response()->json($dataLayer->execute($request));
    }
}
