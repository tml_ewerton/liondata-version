<?php

namespace Lion\Sdk;
use Facebook\Facebook;

class FacebookSdk
{

    private $fb;

    public function __construct($accessToken = null)
    {
        $this->fb = new Facebook([
            'app_id' => env('FACEBOOK_ID'),
            'app_secret' => env('FACEBOOK_SECRET'),
            'default_graph_version' => 'v3.2',
            'default_access_token' => $accessToken ?? null,
        ]);
    }

    public function getUserInfo($userId)
    {
        $res = $this->fb->get('/' . $userId . '?fields=id,name,email');
        return $res->getGraphUser();
    }

}