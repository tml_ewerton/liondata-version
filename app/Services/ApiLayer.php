<?php

namespace Lion\Services;

use Lion\ApiToken;
use Lion\CustomerApi;

class ApiLayer
{

    public function __construct()
    {

    }

    public function auth($username, $password, $scope)
    {
        $customer = CustomerApi::where('username', $username)
            ->where('password', $password)
            ->where('scope', $scope)->first();

        if ($customer) {
            return $this->generateToken($customer);
        }
    }

    public function generateToken($customer)
    {
        return ApiToken::create([
            'customer_api_id' => $customer->id,
            'token' => md5(uniqid() . now() . str_random(16)) .
                       '_' . md5($customer->id . $customer->username .
                                     $customer->password . $customer->scope),
            'scope' => $customer->scope,
            'created' => now(),
            'lifetime' => 1
        ]);
    }

    public function hasActiveToken($id, $scope)
    {
        $token = ApiToken::where('customer_api_id', $id)
                         ->where('scope', $scope)
                         ->first();

        if ($token) {

        }
    }

    public function validateToken($token)
    {

    }



}