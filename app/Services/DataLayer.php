<?php

namespace Lion\Services;

use Illuminate\Http\Request;

class DataLayer
{

    private $model;

    private $relationship;

    private $expressions = [
        'where' => 'where',
        'in' => 'whereIn',
        'group' => 'groupBy',
        'order' => 'orderBy',
    ];

    public function __construct($model, $relationship = [])
    {
        $this->model = $model;
        $this->relationship = $relationship;
    }

    public function execute($request, $perpage = null)
    {
        $this->model = $this->model->with($this->relationship);

        foreach ($this->expressions as $exp => $met) {
            if (isset($request->all()[$exp])) {
                $this->model = $this->$met($request->all()[$exp]);
            }
        }

        if ($perpage === null) {
            return $this->model->get();
        }

        return $this->model->paginate($perpage);
    }

    /**
     * @param $data
     *
     * @return $this
     */
    private function where($data)
    {
        $dt = explode(',', $data);
        $ex = explode('|', $dt[1]);

        return $this->model->whereIn($dt[0], $ex);
    }

    /**
     * @param $data
     *
     * @return $this
     */
    public function whereIn($data)
    {
        $dt = explode('|', $data);
        $d = explode(',', $dt[1]);
        $this->model->whereIn($dt[0], $d);
        return $this;
    }

    /**
     * @param $data
     *
     * @return $this
     */
    public function groupBy($data)
    {
        $this->model->groupBy($data);
        return $this;
    }

    /**
     *
     *
     * @param $data
     *
     * @return $this
     */
    public function orderBy($data)
    {
        $dt = explode(',', $data);

        $this->model->orderBy($dt[0], $dt[1]);
        return $this;
    }

    public function paginate($num)
    {
        return $this->model->paginate($num);
    }

    public function get()
    {
        return $this->model->get();
    }

}