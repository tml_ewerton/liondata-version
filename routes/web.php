<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);
Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth', 'web', 'verified']], function() {
    Route::get('/', 'HomeController@index')->name('home');

    include_once base_path('routes/modules/customers.php');
});

Route::any('webhook/facebook', 'WebhookController@facebook');

/**
 * Get the localization based in user language.
 */
Route::get('/js/lang.js', function () {

    // If env is development, ever forget the lang.js
    if (env('APP_ENV') === 'local') {
        cache()->forget('lang.js');
    }

    $strings = cache()->rememberForever('lang.js', function () {
        $lang = auth()->user()->language ?? config('app.locale');
        $files   = glob(resource_path('lang/' . $lang . '/*.php'));
        $strings = [];

        foreach ($files as $file) {
            $name           = basename($file, '.php');
            $strings[$name] = require $file;
        }

        return $strings;
    });

    header('Content-Type: text/javascript');
    echo('window.i18n = ' . json_encode($strings) . ';');
    exit();
})->name('assets.lang');

Route::get('welcome-email', function() {
    \Illuminate\Support\Facades\Mail::to(
        'developerewt@gmail.com'
    )->send(new \Lion\Mail\Welcome());
});

Route::get('test', function() {
    return view('mail.store-facebook-user');
});