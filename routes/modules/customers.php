<?php

// Customers
Route::get('customers/data', 'CustomerController@data');
Route::resource('customers', 'CustomerController');

// Customer Addresses
Route::get('customer-addresses/data', 'CustomerAddressController@data');
Route::resource('customer-addresses', 'CustomerAddressController')->except([
    'index', 'create', 'update'
]);

// Customer Addresses
Route::get('customer-contacts/data', 'CustomerContactController@data');
Route::resource('customer-contacts', 'CustomerContactController')->except([
    'index', 'create', 'update'
]);