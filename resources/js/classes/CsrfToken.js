export default class CsrfToken {
    csrf() {
        return document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    }

}