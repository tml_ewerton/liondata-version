
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.VueI18n = require('vue-i18n')

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.use(window.VueI18n)
Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('ds-index', require('./components/DashboardIndex.vue'));


// Customers
Vue.component('customer-index', require('./components/Customers/Index.vue'))
Vue.component('customer-show', require('./components/Customers/Show.vue'))
Vue.component('customer-create', require('./components/Customers/Create.vue'))
Vue.component('customer-edit', require('./components/Customers/Edit.vue'))
Vue.component('form-customer', require('./components/Customers/FormCustomer.vue'))
Vue.component('form-customer-address', require('./components/Customers/FormAddress.vue'))

Vue.prototype.trans = (string, args) => {
    let value = _.get(window.i18n, string);

    _.eachRight(args, (paramVal, paramKey) => {
        value = _.replace(value, `:${paramKey}`, paramVal);
    });
    return value;
};

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});

// Libs
require('jquery-mask-plugin')
require('moment')
