<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <title>The Marketing Lion</title>

    <style type="text/css">
        .ReadMsgBody{
            width:100%;
            background-color:#ffffff;
        }
        .ExternalClass{
            width:100%;
            background-color:#ffffff;
        }
        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{
            line-height:100%;
        }
        html{
            width:100%;
        }
        body{
            -webkit-text-size-adjust:none;
            -ms-text-size-adjust:none;
            margin:0;
            padding:0;
        }
        table{
            border-spacing:0;
            border-collapse:collapse;
        }
        table td{
            border-collapse:collapse;
            font-weight:bold;
        }
        .yshortcuts a{
            border-bottom:none !important;
        }
        img{
            display:block !important;
        }
        a{
            text-decoration:none;
            color:#26c6da;
        }
        @media only screen and (max-width: 640px){
            body{
                width:auto !important;
            }

        }
        @media only screen and (max-width: 640px){
            table[class=table600]{
                width:450px !important;
            }

        }
        @media only screen and (max-width: 640px){
            table[class=table-container]{
                width:90% !important;
            }

        }
        @media only screen and (max-width: 640px){
            table[class=container2-2]{
                width:47% !important;
                text-align:left !important;
            }

        }
        @media only screen and (max-width: 640px){
            table[class=full-width]{
                width:100% !important;
                text-align:center !important;
            }

        }
        @media only screen and (max-width: 640px){
            img[class=img-full]{
                width:100% !important;
                height:auto !important;
            }

        }
        @media only screen and (max-width: 479px){
            body{
                width:auto !important;
            }

        }
        @media only screen and (max-width: 479px){
            table[class=table600]{
                width:290px !important;
            }

        }
        @media only screen and (max-width: 479px){
            table[class=table-container]{
                width:82% !important;
            }

        }
        @media only screen and (max-width: 479px){
            table[class=container2-2]{
                width:100% !important;
                text-align:left !important;
            }

        }
        @media only screen and (max-width: 479px){
            table[class=full-width]{
                width:100% !important;
                text-align:center !important;
            }

        }
        @media only screen and (max-width: 479px){
            img[class=img-full]{
                width:100% !important;
            }

        }
    </style>
</head>
<body marginwidth="0" marginheight="0" style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" offset="0" topmargin="0" leftmargin="0">
<!-- MAIN A -->
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <!-- Background -->
        <td align="center" bgcolor="#333333" background="https://gallery.mailchimp.com/697e5723244b27a5d29a38806/images/157a8abd-47b8-4c7e-9fdc-fa2f8b189d95.png" style="background-size:cover;background-position:center;">
            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="200" style="font-size:1px;line-height:200px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="font-family:Montserrat, sans-serif;font-size:42px;font-weight:400;color:#000000;line-height:48px;letter-spacing:4px;background-color:#FFFFFF;">
                        DÊ UMA OLHADINHA NA SUA CONTA
                    </td>
                </tr>
                <tr>
                    <td height="30" style="font-size:1px;line-height:30px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="font-family:Montserrat, sans-serif;font-size:15px;font-weight:bold;color:#000000;line-height:24px;letter-spacing:2px;background-color:#FFFFFF;">
                        Obrigado pela confiança
                    </td>
                </tr>
                <!-- Underline -->
                <tr>
                    <td align="center">
                        <table width="75" border="0" cellpadding="0" cellspacing="0">
                            <!-- Edit Underline -->
                            <tr>
                                <td height="30" style="border-bottom:2px solid #ffffff;"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- End Underline -->
                <tr>
                    <td height="30" style="font-size:1px;line-height:30px;"> </td>
                </tr>
                <tr>
                    <td height="200" style="font-size:1px;line-height:200px;"> </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- HEADLINE AND CONTENT -->
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="50" style="font-size:1px;line-height:50px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="font-family:Montserrat, sans-serif;font-size:28px;font-weight:400;color:#333333;letter-spacing:2px;line-height:32px;">
                        AGORA VOCÊ É UM CAÇADOR DE INOVAÇÕES
                    </td>
                </tr>
                <!-- Underline -->
                <tr>
                    <td align="center">
                        <table width="75" border="0" cellpadding="0" cellspacing="0">
                            <!-- Edit Underline -->
                            <tr>
                                <td height="20" style="border-bottom:2px solid #26c6da;"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- End Underline -->
                <tr>
                    <td height="20" style="font-size:20px;line-height:20px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="font-family:Montserrat, sans-serif;font-size:13px;font-weight:400;color:#333333;letter-spacing:2px;line-height:24px;">
                        NOSSO TIME</td>
                </tr>
                <tr>
                    <td height="30" style="font-size:1px;line-height:30px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="font-family:'Open Sans', sans-serif;font-size:13px;font-weight:400;color:#8f96a1;line-height:24px;">
                        Nosso foco é fazer seu projeto, sua empresa e VOCÊ, maior, melhor e mais competitivo. Estamos aqui para servir. Neste email você encontrará várias informações sobre como será seu processo.  </td>
                </tr>
                <tr>
                    <td height="50" style="font-size:1px;line-height:50px;"> </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END HEADLINE AND CONTENT -->

</body>
</html>