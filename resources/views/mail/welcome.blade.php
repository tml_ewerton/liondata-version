<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <title>The Marketing Lion</title>

    <style type="text/css">
        .ReadMsgBody{
            width:100%;
            background-color:#ffffff;
        }
        .ExternalClass{
            width:100%;
            background-color:#ffffff;
        }
        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{
            line-height:100%;
        }
        html{
            width:100%;
        }
        body{
            -webkit-text-size-adjust:none;
            -ms-text-size-adjust:none;
            margin:0;
            padding:0;
        }
        table{
            border-spacing:0;
            border-collapse:collapse;
        }
        table td{
            border-collapse:collapse;
            font-weight:bold;
        }
        .yshortcuts a{
            border-bottom:none !important;
        }
        img{
            display:block !important;
        }
        a{
            text-decoration:none;
            color:#26c6da;
        }
        @media only screen and (max-width: 640px){
            body{
                width:auto !important;
            }

        }	@media only screen and (max-width: 640px){
            table[class=table600]{
                width:450px !important;
            }

        }	@media only screen and (max-width: 640px){
            table[class=table-container]{
                width:90% !important;
            }

        }	@media only screen and (max-width: 640px){
            table[class=container2-2]{
                width:47% !important;
                text-align:left !important;
            }

        }	@media only screen and (max-width: 640px){
            table[class=full-width]{
                width:100% !important;
                text-align:center !important;
            }

        }	@media only screen and (max-width: 640px){
            img[class=img-full]{
                width:100% !important;
                height:auto !important;
            }

        }	@media only screen and (max-width: 479px){
            body{
                width:auto !important;
            }

        }	@media only screen and (max-width: 479px){
            table[class=table600]{
                width:290px !important;
            }

        }	@media only screen and (max-width: 479px){
            table[class=table-container]{
                width:82% !important;
            }

        }	@media only screen and (max-width: 479px){
            table[class=container2-2]{
                width:100% !important;
                text-align:left !important;
            }

        }	@media only screen and (max-width: 479px){
            table[class=full-width]{
                width:100% !important;
                text-align:center !important;
            }

        }	@media only screen and (max-width: 479px){
            img[class=img-full]{
                width:100% !important;
            }

        }</style></head>
<body marginwidth="0" marginheight="0" style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" offset="0" topmargin="0" leftmargin="0">
<!-- MAIN A -->
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <!-- Background -->
        <td align="center" bgcolor="#333333" background="https://gallery.mailchimp.com/697e5723244b27a5d29a38806/images/157a8abd-47b8-4c7e-9fdc-fa2f8b189d95.png" style="background-size:cover;background-position:center;">
            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="200" style="font-size:1px;line-height:200px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="font-family:Montserrat, sans-serif;font-size:42px;font-weight:400;color:#000000;line-height:48px;letter-spacing:4px;background-color:#FFFFFF;">
                        BEM VINDO LEÕES</td>
                </tr>
                <tr>
                    <td height="30" style="font-size:1px;line-height:30px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="font-family:Montserrat, sans-serif;font-size:15px;font-weight:bold;color:#000000;line-height:24px;letter-spacing:2px;background-color:#FFFFFF;">
                        Obrigado pela confiança</td>
                </tr>
                <!-- Underline -->
                <tr>
                    <td align="center">
                        <table width="75" border="0" cellpadding="0" cellspacing="0">
                            <!-- Edit Underline -->
                            <tr>
                                <td height="30" style="border-bottom:2px solid #ffffff;"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- End Underline -->
                <tr>
                    <td height="30" style="font-size:1px;line-height:30px;"> </td>
                </tr>
                <tr>
                    <td height="200" style="font-size:1px;line-height:200px;"> </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END MAIN A -->
<!-- END SPACE -->
<!-- HEADLINE AND CONTENT -->
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="50" style="font-size:1px;line-height:50px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="font-family:Montserrat, sans-serif;font-size:28px;font-weight:400;color:#333333;letter-spacing:2px;line-height:32px;">
                        AGORA VOCÊ É UM CAÇADOR DE INOVAÇÕES </td>
                </tr>
                <!-- Underline -->
                <tr>
                    <td align="center">
                        <table width="75" border="0" cellpadding="0" cellspacing="0">
                            <!-- Edit Underline -->
                            <tr>
                                <td height="20" style="border-bottom:2px solid #26c6da;"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- End Underline -->
                <tr>
                    <td height="20" style="font-size:20px;line-height:20px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="font-family:Montserrat, sans-serif;font-size:13px;font-weight:400;color:#333333;letter-spacing:2px;line-height:24px;">
                        NOSSO TIME</td>
                </tr>
                <tr>
                    <td height="30" style="font-size:1px;line-height:30px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="font-family:'Open Sans', sans-serif;font-size:13px;font-weight:400;color:#8f96a1;line-height:24px;">
                        Nosso foco é fazer seu projeto, sua empresa e VOCÊ, maior, melhor e mais competitivo. Estamos aqui para servir. Neste email você encontrará várias informações sobre como será seu processo.  </td>
                </tr>
                <tr>
                    <td height="50" style="font-size:1px;line-height:50px;"> </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END HEADLINE AND CONTENT -->
<!-- FEATURES 3 -->
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="50" style="font-size:1px;line-height:50px;"> </td>
                </tr>
                <tr>
                    <td>
                        <table class="full-width" width="183" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;">
                            <tr>
                                <td align="center">
                                    <img src="https://gallery.mailchimp.com/697e5723244b27a5d29a38806/images/854d14b4-ec2a-4590-862c-55e782231f24.png" alt="icon" width="64" height="64">
                                </td>
                            </tr>
                            <tr>
                                <td height="25" style="font-size:1px;line-height:25px;"> </td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family:Montserrat, sans-serif;font-size:15px;font-weight:700;color:#333333;line-height:24px;letter-spacing:1px;">
                                    SERVIÇO AO CONSUMIDOR</td>
                            </tr>
                            <tr>
                                <td height="25" style="font-size:1px;line-height:25px;"> </td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family:'Open Sans', sans-serif;font-size:13px;font-weight:400;color:#8f96a1;line-height:24px;">
                                    Estamos aqui para ajudar você. Tem dúvidas sobre o processo? Nāo hesite em entrar em contato com nossos leões.<br> +1 339-927-7803</td>
                            </tr>
                        </table>
                        <!-- SPACE -->
                        <table class="full-width" width="24" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;">
                            <tr>
                                <td width="24" height="30" style="font-size:30px;line-height:30px;"></td>
                            </tr>
                        </table>
                        <!-- END SPACE -->
                        <table class="full-width" width="183" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;">
                            <tr>
                                <td align="center">
                                    <img src="https://gallery.mailchimp.com/697e5723244b27a5d29a38806/images/98db3393-968b-489a-b683-f461aa9197db.png" alt="icon" width="64" height="64">
                                </td>
                            </tr>
                            <tr>
                                <td height="25" style="font-size:1px;line-height:25px;"> </td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family:Montserrat, sans-serif;font-size:15px;font-weight:700;color:#333333;line-height:24px;letter-spacing:1px;">
                                    PROCESSO DE DESENVOLVIMENTO</td>
                            </tr>
                            <tr>
                                <td height="25" style="font-size:1px;line-height:25px;"> </td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family:'Open Sans', sans-serif;font-size:13px;font-weight:400;color:#8f96a1;line-height:24px;">
                                    Estamos criando e cuidando do seu processo de crescimento. O seu representante pessoal entrará em contato com você para escutar atenciosamente suas visões.</td>
                            </tr>
                        </table>
                        <!-- SPACE -->
                        <table class="full-width" width="1" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;">
                            <tr>
                                <td width="1" height="30" style="font-size:30px;line-height:30px;"></td>
                            </tr>
                        </table>
                        <!-- END SPACE -->
                        <table class="full-width" width="183" align="right" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;">
                            <tr>
                                <td align="center">
                                    <img src="https://gallery.mailchimp.com/697e5723244b27a5d29a38806/images/5bf49d41-3745-4c69-814f-a5072800289d.png" alt="icon" width="64" height="64">
                                </td>
                            </tr>
                            <tr>
                                <td height="25" style="font-size:1px;line-height:25px;"> </td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family:Montserrat, sans-serif;font-size:15px;font-weight:700;color:#333333;line-height:24px;letter-spacing:1px;">
                                    QUESTIONÁRIOS</td>
                            </tr>
                            <tr>
                                <td height="25" style="font-size:1px;line-height:25px;"> </td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family:'Open Sans', sans-serif;font-size:13px;font-weight:400;color:#8f96a1;line-height:24px;">
                                    Sua contribuiçāo e opiniāo, sāo muito importantes para estabelecer os valores e identidade desejada do seu projeto. Nos próximos dias você receberá questionários sobre seus serviços selecionados.</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50" style="font-size:1px;line-height:50px;"> </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END FEATURES 3 -->
<!-- SEPERATOR A -->
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <!-- Background -->
        <td align="center" bgcolor="#333333" background="https://gallery.mailchimp.com/697e5723244b27a5d29a38806/images/3f520f11-7d34-4e27-9156-7bbc6b41fc9a.jpg" style="background-size:cover;background-position:center;">
            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="75" style="font-size:1px;line-height:75px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="font-family:Montserrat, sans-serif;font-size:28px;font-weight:400;color:#ffffff;letter-spacing:2px;line-height:32px;">
                        FUNDADORES</td>
                </tr>
                <!-- Underline -->
                <tr>
                    <td align="center">
                        <table width="75" border="0" cellpadding="0" cellspacing="0">
                            <!-- Edit Underline -->
                            <tr>
                                <td height="20" style="border-bottom:2px solid #26c6da;"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- End Underline -->
                <tr>
                    <td height="20" style="font-size:20px;line-height:20px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="font-family:Montserrat, sans-serif;font-size:13px;font-weight:400;color:#ffffff;letter-spacing:2px;line-height:24px;">
                        FERNANDA LIBARES / CLAUDIO SENA / ARTHUR BRAGA</td>
                </tr>
                <tr>
                    <td height="75" style="font-size:1px;line-height:75px;"> </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END SEPERATOR A -->
<!-- ARTICLE RIGHT -->
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="50" style="font-size:1px;line-height:50px;"> </td>
                </tr>
                <tr>
                    <td>

                        <table class="full-width" width="287" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;">
                            <tr>
                                <td align="center">
                                    <img class="img-full" src="https://gallery.mailchimp.com/697e5723244b27a5d29a38806/images/d7d1d6df-db89-4a9a-9bd1-412c08dc140f.png" alt="img" width="287" height="192">
                                </td>
                            </tr>
                        </table>
                        <!-- SPACE -->
                        <table class="full-width" width="1" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;">
                            <tr>
                                <td width="1" height="40" style="font-size:40px;line-height:40px;"></td>
                            </tr>
                        </table>
                        <!-- END SPACE -->
                        <table class="full-width" width="287" align="right" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;">
                            <tr>
                                <td style="font-family:Montserrat, sans-serif;font-size:24px;font-weight:400;color:#333333;letter-spacing:1px;line-height:32px;">
                                    DESENVOLVIMENTO DE APLICATIVOS</td>
                            </tr>
                            <tr>
                                <td height="10" style="font-size:1px;line-height:10px;"> </td>
                            </tr>
                            <tr>
                                <td style="font-family:Montserrat, sans-serif;font-size:10px;font-weight:700;color:#26c6da;letter-spacing:2px;line-height:18px;">
                                </td>
                            </tr>
                            <tr>
                                <td height="15" style="font-size:1px;line-height:15px;"> </td>
                            </tr>
                            <tr>
                                <td style="font-family:'Open Sans', sans-serif;font-size:13px;font-weight:400;color:#8f96a1;line-height:24px;">
                                    Tem uma idéia transfomadora para um aplicativo? Nosso time de desenvolvedores web estāo prontos para auxiliar você a tirar ela do papel e revolucionar novos segmentos.  </td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size:1px;line-height:20px;"> </td>
                            </tr>
                            <tr>
                                <td style="font-family:'Open Sans', sans-serif;font-size:13px;font-weight:600;color:#26c6da;line-height:24px;">
                                    <a href="#" style="text-decoration:none;color:#26c6da;">SABER MAIS</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50" style="font-size:1px;line-height:50px;"> </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END ARTICLE RIGHT -->
<!-- ARTICLE LEFT -->
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="50" style="font-size:1px;line-height:50px;"> </td>
                </tr>
                <tr>
                    <td>

                        <table class="full-width" width="287" align="right" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;">
                            <tr>
                                <td align="center">
                                    <img class="img-full" src="https://gallery.mailchimp.com/697e5723244b27a5d29a38806/images/58d6442e-a682-44a1-ac33-c50bf2584ab5.png" alt="img" width="287" height="192">
                                </td>
                            </tr>
                        </table>
                        <!-- SPACE -->
                        <table class="full-width" width="1" align="right" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;">
                            <tr>
                                <td width="1" height="40" style="font-size:40px;line-height:40px;"></td>
                            </tr>
                        </table>
                        <!-- END SPACE -->
                        <table class="full-width" width="287" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;">
                            <tr>
                                <td style="font-family:Montserrat, sans-serif;font-size:24px;font-weight:400;color:#333333;letter-spacing:1px;line-height:32px;">
                                    PRODUÇĀO DE VÍDEO</td>
                            </tr>
                            <tr>
                                <td height="10" style="font-size:1px;line-height:10px;"> </td>
                            </tr>
                            <tr>
                                <td style="font-family:Montserrat, sans-serif;font-size:10px;font-weight:700;color:#26c6da;letter-spacing:2px;line-height:18px;">
                                </td>
                            </tr>
                            <tr>
                                <td height="15" style="font-size:1px;line-height:15px;"> </td>
                            </tr>
                            <tr>
                                <td style="font-family:'Open Sans', sans-serif;font-size:13px;font-weight:400;color:#8f96a1;line-height:24px;">
                                    Pronto para uma experiência de cair o queixo? Vídeos profissionais personalizados a identidade da sua empresa. Junte-se aos nossos produtores e tenha uma experiência que nāo só vende você e seu negócio, mas agrega valor a sua marca em todas as etapas do processo.  </td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size:1px;line-height:20px;"> </td>
                            </tr>
                            <tr>
                                <td style="font-family:'Open Sans', sans-serif;font-size:13px;font-weight:600;color:#26c6da;line-height:24px;">
                                    <a href="#" style="text-decoration:none;color:#26c6da;">SABER MAIS</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50" style="font-size:1px;line-height:50px;"> </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END ARTICLE LEFT -->
<!-- PRICE 2 -->
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <!-- Background -->
        <td align="center" bgcolor="#edf0f7">
            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="50" style="font-size:1px;line-height:50px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="font-family:Montserrat, sans-serif;font-size:28px;font-weight:400;color:#333333;letter-spacing:2px;line-height:32px;">
                        COMO VENDER MAIS?</td>
                </tr>
                <!-- Underline -->
                <tr>
                    <td align="center">
                        <table width="75" border="0" cellpadding="0" cellspacing="0">
                            <!-- Edit Underline -->
                            <tr>
                                <td height="20" style="border-bottom:2px solid #26c6da;"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- End Underline -->
                <tr>
                    <td height="20" style="font-size:20px;line-height:20px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="font-family:Montserrat, sans-serif;font-size:13px;font-weight:400;color:#333333;letter-spacing:2px;line-height:24px;">
                        FERRAMENTAS PODEROSAS</td>
                </tr>
                <tr>
                    <td height="30" style="font-size:1px;line-height:30px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="font-family:'Open Sans', sans-serif;font-size:13px;font-weight:400;color:#8f96a1;line-height:24px;">
                        Se prepare para enxergar você, sua empresa e seu time com lentes completamente diferentes. Análise de perfil comportamental ajuda você a mapear e selecionar mentes para as funções certas. Assim poderá vender mais, aumentando sua produtividade pessoal e de seu time, maximizando seu potencial humano.   </td>
                </tr>
                <tr>
                    <td height="40" style="font-size:1px;line-height:40px;"> </td>
                </tr>
                <tr>
                    <td>
                        <table class="full-width" width="287" align="left" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;">
                            <tr>
                                <td height="30" style="font-size:1px;line-height:30px;"> </td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family:Montserrat, sans-serif;font-size:24px;font-weight:400;color:#333333;letter-spacing:1px;line-height:28px;">
                                    ANÁLISE INDIVIDUAL</td>
                            </tr>
                            <!-- Underline -->
                            <tr>
                                <td align="center">
                                    <table width="75" border="0" cellpadding="0" cellspacing="0">
                                        <!-- Edit Underline -->
                                        <tr>
                                            <td height="20" style="border-bottom:2px solid #26c6da;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!-- End Underline -->
                            <tr>
                                <td height="20" style="font-size:20px;line-height:20px;"> </td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family:Montserrat, sans-serif;font-size:13px;font-weight:400;color:#333333;letter-spacing:1px;line-height:24px;">
                                    DESCUBRA SEU VERDADEIRO POTENCIAL</td>
                            </tr>
                            <tr>
                                <td height="30" style="font-size:1px;line-height:30px;"> </td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family:Montserrat, sans-serif;font-size:58px;font-weight:300;color:#333333;line-height:60px;">
                                    $399</td>
                            </tr>
                            <tr>
                                <td height="10" style="font-size:1px;line-height:10px;"> </td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family:'Open Sans', sans-serif;font-size:13px;font-weight:400;color:#8f96a1;line-height:24px;">
                                    Análise personalizada com relatório
                                    &amp; devolutiva pessoal.</td>
                            </tr>
                            <tr>
                                <td height="30" style="font-size:1px;line-height:30px;"> </td>
                            </tr>
                            <!-- Button -->
                            <tr>
                                <td align="center">
                                    <table width="170" align="center" border="0" cellpadding="0" cellspacing="0" style="border-radius:2px;" bgcolor="#26c6da">
                                        <tr>
                                            <td height="10" style="font-size:1px;line-height:10px;"> </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="font-family:'Open Sans', sans-serif;font-size:14px;font-weight:600;color:#ffffff;line-height:24px;">
                                                <a href="#" style="color:#ffffff;text-decoration:none;">Descubra mais</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="font-size:1px;line-height:10px;"> </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!-- End Button -->
                            <tr>
                                <td height="30" style="font-size:1px;line-height:30px;"> </td>
                            </tr>
                        </table>
                        <!-- SPACE -->
                        <table class="full-width" width="1" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;">
                            <tr>
                                <td width="1" height="40" style="font-size:40px;line-height:40px;"></td>
                            </tr>
                        </table>
                        <!-- END SPACE -->
                        <table class="full-width" width="287" align="right" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;">
                            <tr>
                                <td height="30" style="font-size:1px;line-height:30px;"> </td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family:Montserrat, sans-serif;font-size:24px;font-weight:400;color:#333333;letter-spacing:1px;line-height:28px;">
                                    TIME EMPRESARIAL (CADA)</td>
                            </tr>
                            <!-- Underline -->
                            <tr>
                                <td align="center">
                                    <table width="75" border="0" cellpadding="0" cellspacing="0">
                                        <!-- Edit Underline -->
                                        <tr>
                                            <td height="20" style="border-bottom:2px solid #26c6da;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!-- End Underline -->
                            <tr>
                                <td height="20" style="font-size:20px;line-height:20px;"> </td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family:Montserrat, sans-serif;font-size:13px;font-weight:400;color:#333333;letter-spacing:1px;line-height:24px;">
                                    ENTENDA SEU TIME E SAIBA QUEM CONTRATAR</td>
                            </tr>
                            <tr>
                                <td height="30" style="font-size:1px;line-height:30px;"> </td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family:Montserrat, sans-serif;font-size:58px;font-weight:300;color:#333333;line-height:60px;">
                                    $200</td>
                            </tr>
                            <tr>
                                <td height="10" style="font-size:1px;line-height:10px;"> </td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family:'Open Sans', sans-serif;font-size:13px;font-weight:400;color:#8f96a1;line-height:24px;">
                                    Descubra como potencializar seu time. Identifique as áreas de melhor desempenho para cada perfil. Relatório e devolutiva individual.</td>
                            </tr>
                            <tr>
                                <td height="30" style="font-size:1px;line-height:30px;"> </td>
                            </tr>
                            <!-- Button -->
                            <tr>
                                <td align="center">
                                    <table width="170" align="center" border="0" cellpadding="0" cellspacing="0" style="border-radius:2px;" bgcolor="#26c6da">
                                        <tr>
                                            <td height="10" style="font-size:1px;line-height:10px;"> </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="font-family:'Open Sans', sans-serif;font-size:14px;font-weight:600;color:#ffffff;line-height:24px;">
                                                <a href="#" style="color:#ffffff;text-decoration:none;">Descubra mais</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="font-size:1px;line-height:10px;"> </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!-- End Button -->
                            <tr>
                                <td height="30" style="font-size:1px;line-height:30px;"> </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="75" style="font-size:1px;line-height:75px;"> </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END PRICE 2 --> 
<!-- END ARTICLE FULL -->
<!-- MAIN B -->
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <!-- Background -->
        <td align="center" bgcolor="#333333" background="https://gallery.mailchimp.com/697e5723244b27a5d29a38806/images/157a8abd-47b8-4c7e-9fdc-fa2f8b189d95.png" style="background-size:cover;background-position:center;">
            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                <tr>
                    <td height="200" style="font-size:1px;line-height:200px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="font-family:Montserrat, sans-serif;font-size:42px;font-weight:400;color:#000000;line-height:48px;letter-spacing:4px;background-color:#FFFFFF;">
                        50 Milk Street, MA, 02110</td>
                </tr>
                <tr>
                    <td height="30" style="font-size:1px;line-height:30px;"> </td>
                </tr>
                <tr>
                    <td align="center" style="font-family:Montserrat, sans-serif;font-size:15px;font-weight:bold;color:#000000;line-height:24px;letter-spacing:2px;background-color:#FFFFFF;">
                        www.themarketinglionagency.com</td>
                </tr>
                <!-- Underline -->
                <tr>
                    <td align="center">
                        <table width="75" border="0" cellpadding="0" cellspacing="0">
                            <!-- Edit Underline -->
                            <tbody>
                            <tr>
                                <td height="30" style="border-bottom:2px solid #000000;"></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <!-- End Underline -->
                <tr>
                    <td height="30" style="font-size:1px;line-height:30px;"> </td>
                </tr>
                <tr>
                    <td height="200" style="font-size:1px;line-height:200px;"> </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td height="90" style="font-size:1px;line-height:90px;"> </td>
        <td align="center" style="font-family:Montserrat, sans-serif;font-size:15px;font-weight:400;color:#ffffff;line-height:24px;letter-spacing:2px;">
        </td>
        <td height="30" style="font-size:1px;line-height:30px;"> </td>
        <td align="center" style="font-family:Montserrat, sans-serif;font-size:42px;font-weight:400;color:#ffffff;line-height:48px;letter-spacing:4px;">
        </td>
        <!-- Underline -->
        <td align="center">
            <table width="0" border="0" cellpadding="0" cellspacing="0">
                <!-- Edit Underline -->
                <tr>
                    <td height="30" style="border-bottom:2px solid #ffffff;"></td>
                </tr>
            </table>
        </td>
        <!-- End Underline -->
        <td height="30" style="font-size:1px;line-height:30px;"> </td>
        <td height="90" style="font-size:1px;line-height:90px;"> </td>
    </tr>
</table>
</body>
</html>