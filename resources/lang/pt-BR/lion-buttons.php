<?php

return [
    'save'              => 'Salvar',
    'edit'              => 'Editar',
    'delete'            => 'Deletar',
    'restore'           => 'Restaurar',
    'see'               => 'Ver',
    'new'               => 'Novo',
    'close'             => 'Fechar'
];
