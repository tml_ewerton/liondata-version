<?php

return [
    'customers' => 'Clientes',
    'customer' => 'Cliente',

    'basic_info' => 'Informações básicas',
    'name' => 'Nome',
    'company_name' => 'Nome da Empresa',
    'document' => 'Documento',
    'picture' => 'Imagem',
    'about' => 'Sobre',
    'slug' => 'Slug',
    'website' => 'Website',

    'title_delete_customer' => 'Deletar cliente',
    'message_delete_customer' => 'Tem certeza que deseja desativar esse cliente? Caso ele tenha serviços ativos, os mesmos também serão desativados.',

    'addresses' => 'Endereços',
    'address' => 'Endereço',
    'address_type' => 'Tipo de endereço',
    'address' => 'Endereço',
    'number' => 'Número',
    'complement' => 'Complemento',
    'neighborhood' => 'Bairro',
    'city' => 'Cidade',
    'state' => 'Estado',
    'zip_code' => 'Código postal',
    'code' => 'Código do endereço',

    'address_type_main' => 'Principal',
    'address_type_mail' => 'Correspondência',

    'title_create_customer_address' => 'Adicionar novo endereço',
    'message_no_address' => 'Não há endereço cadastrado para esse cliente.',
];